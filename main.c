#include <sys/socket.h>
#include <netinet/in.h>
#include <assert.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include "libft.h"
#include <stdio.h>
#include <netinet/ip_icmp.h>
#include <unistd.h>
#include <netinet/udp.h>
#include "ft_traceroute.h"

t_sys *init(int ac, char **av)
{
	t_sys *sys = (t_sys *)malloc(sizeof(t_sys));
	if (!sys)
		return (NULL);

	sys->max_hops = 30;
	sys->reached_last_hop = 0;
	sys->packet_size = 32;
	sys->timeout = 1;
	sys->src_addr = NULL;
	sys->flag_first_probe = 0;
	// set options
	int ret = set_options(sys, ac, av);
	if (ret == 0) {
		printf("traceroute: usage error: wrong options (-h to list supported options) \n");
		exit(1);
	}
	else if (ret == -1) {
		printf("traceroute: usage error: wrong options (-h to list supported options) \n");
		exit(1);
	}

	// prepare sockets
	ret = prepare_sockets(sys);
	if (!ret) {
		exit(1);
	}

	// init ttl
	ret = set_ttl(sys, 1);
	if (!ret) {
		exit(1);
	}
	sys->ip_addr = NULL;
	// resolve address
	ret = resolve_addr(sys, sys->addr);
	if (!ret) {
		printf("traceroute: %s: Name or service not known\n", sys->addr);
		exit(1);
	}

	// tout va bien
	return (sys);
}

int main(int ac, char **av) {
	uid_t uid = getuid();
	if (uid != 0) {
		printf("traceroute: uid not root, need root to use raw sockets \n");
		return (1);
	}

	t_sys *sys = init(ac, av);
	if (!sys)
		return (1);
	printf("traceroute to ");
	printf("%s (%s), ", sys->addr, sys->ip_addr);
	printf("%d hops max, ", sys->max_hops);
	printf("%lu byte packets\n", sys->packet_size + sizeof(struct iphdr) + sizeof(struct udphdr));
	int hops = 0;

	while (hops < sys->max_hops) {
		printf("%*d ", 2, hops + 1);
		sys->flag_first_probe = 1;
		udp_probe(sys, UDP_PROBE, hops + 1);
		sys->flag_first_probe = 0;
		udp_probe(sys, UDP_PROBE, hops + 1);
		udp_probe(sys, UDP_PROBE, hops + 1);
		printf("\n");
		if (sys->reached_last_hop == 1)
			break;
		hops++;
	}
	exit(0);
}
