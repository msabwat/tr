#include "ft_traceroute.h"
#include "libft.h"
#include <stdio.h>

int set_options(t_sys *sys, int ac, char **av)
{
	if (ac == 1)
		return (0);
	else if (ac == 2) {
		if (ft_strcmp(av[1], "-h") == 0) {
			printf("traceroute: ./ft_traceroute <ip address | hostname>\n");
			printf("Need to run as root to be able to use raw sockets\n");
			return (0);
		}
		else {
			sys->addr = av[1];
		}
	}
	else {
		return (-1);
	}
	// success
	return (1);
}
