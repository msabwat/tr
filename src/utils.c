#include <arpa/inet.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdio.h>
#include "ft_traceroute.h"
#include "libft.h"

int	resolve_addr(t_sys *sys, const char *addr)
{
	struct addrinfo hints;
	struct addrinfo *result;

	char *new = malloc(sizeof(char) * 100);
	if (!new)
		return (0);
	ft_memset(&hints, 0, sizeof(struct addrinfo));
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_RAW;
	hints.ai_flags = AI_CANONNAME;
	hints.ai_protocol = 0;
	hints.ai_canonname = NULL;
	hints.ai_addr = NULL;
	hints.ai_next = NULL;
	
	int ret = getaddrinfo(addr, NULL, &hints, &result);
	if (ret != 0) {
		free(new);
		return (0);
	}
	// to do: what if address is valid but not AF_INET (should be able to do ->next until you find the right one before you bail out)
	inet_ntop (result->ai_family, &((struct sockaddr_in *)result->ai_addr)->sin_addr, new, 100);
	sys->dest_addr = result->ai_addr;
	sys->ip_addr = new;

	freeaddrinfo(result);
	return (1);
}

int prepare_sockets(t_sys *sys) {
	sys->recv_socket = socket(AF_INET, SOCK_RAW, IPPROTO_ICMP);
	if (sys->recv_socket == -1) {
		return (0);
	}
	sys->send_socket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if (sys->send_socket == -1) {
		return (0);
	}
	struct timeval tv;
	tv.tv_sec = sys->timeout;
	tv.tv_usec = 0;
	if (setsockopt(sys->recv_socket, SOL_SOCKET, SO_RCVTIMEO,(struct timeval *)&tv,sizeof(struct timeval)) < 0) {
		printf("traceroute: unable to set SO_RCVTIME0 option\n");
		return (0);
	}

	// pas d'erreur
	return (1);
}

int set_ttl(t_sys *sys, int ttl) {
	if ((ttl > 255) || (ttl <= 0)) {
		printf("traceroute: error requesting invalid ttl\n");
		return (0);
	}
	if (setsockopt(sys->send_socket, IPPROTO_IP, IP_TTL, &ttl, sizeof(ttl)) < 0) {
		printf("traceroute: unable to set IP_TTL option\n");
		return (0);
	}
	return (1);
}
