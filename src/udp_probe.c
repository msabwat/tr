#include "ft_traceroute.h"
#include "libft.h"
#include <netinet/in.h>
#include <netinet/ip_icmp.h>
#include <arpa/inet.h>
#include <sys/time.h>
#include <stdio.h>

void udp_probe(t_sys *sys, t_probe probe_type, int ttl) {
	struct timeval time;
	long start_time = 0;
	long end_time = 0;

	if (probe_type != UDP_PROBE)
		return;
	int ret = set_ttl(sys, ttl);
	if (!ret)
		return ;
	size_t size = sys->packet_size;
	struct sockaddr_in target_addr;
	target_addr.sin_family = AF_INET;
	target_addr.sin_port = htons(33433 + ttl);
	target_addr.sin_addr.s_addr = 0;
	inet_pton (AF_INET, sys->ip_addr, &target_addr.sin_addr);
	char s_buffer[size];
	ft_bzero(s_buffer, size);
	gettimeofday(&time, NULL);
	start_time = time.tv_sec * 1000000 + time.tv_usec;
	sendto(sys->send_socket, &s_buffer[0], size, 0, (struct sockaddr *) &target_addr, sizeof (struct sockaddr_in));
	char r_buffer[sizeof(struct iphdr) + sizeof(struct icmphdr) + size];
	unsigned int addrlen = sizeof(struct sockaddr_in);
	recvfrom(sys->recv_socket, r_buffer, sizeof(struct iphdr) + sizeof(struct icmphdr) + size,
			 0, (struct sockaddr *) &target_addr, &addrlen);
	gettimeofday(&time, NULL);
	end_time = (time.tv_sec * 1000000 + time.tv_usec) - start_time;
	struct iphdr *ip_reply = (struct iphdr *)r_buffer;
	struct icmphdr *icmp_reply = (struct icmphdr *)(r_buffer + ip_reply->ihl*4);
	if (end_time / 1000l >= sys->timeout * 1000) {
		printf(" *");
		return ;
	}
	char new_addr[16];
	inet_ntop(AF_INET, &ip_reply->saddr, new_addr, sizeof(new_addr));
	if (!sys->src_addr) {
		sys->src_addr = ft_strnew(16);
		if(!sys->addr) {
			printf("error, cannot allocate 16 bytes");
			exit(1);
		}
		ft_strncpy(sys->src_addr, new_addr, 16);
	}
	else {
		if (ft_strcmp(new_addr, sys->src_addr) != 0) {
			ft_strncpy(sys->src_addr, new_addr, 16);
		}
	}
	
	if (icmp_reply->type == ICMP_TIME_EXCEEDED) {
		if (((ft_strcmp(new_addr, sys->src_addr) == 0) && (sys->flag_first_probe == 1)) || (ft_strcmp(new_addr, sys->src_addr) != 0))
			printf(" %s ", sys->src_addr);
		printf(" %ld.%03ld ms", end_time / 1000l, end_time % 1000l);
	}
	else if (icmp_reply->type == ICMP_DEST_UNREACH) {
		if (((ft_strcmp(new_addr, sys->src_addr) == 0) && (sys->flag_first_probe == 1)) || (ft_strcmp(new_addr, sys->src_addr) != 0))
			printf(" %s ", sys->src_addr);
		printf(" %ld.%03ld ms", end_time / 1000l, end_time % 1000l);
		sys->reached_last_hop = 1;
	}
}
