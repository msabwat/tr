#include <stddef.h>
#include <unistd.h>

typedef enum e_probe
{
	UDP_PROBE,
	TCP_PROBE,
	ICMP_PROBE
} t_probe;

typedef struct s_sys
{
	const char *addr;
	const char *ip_addr;
	struct addrinfo *addr_info;
	struct sockaddr *dest_addr;
	char reached_last_hop;
	char *src_addr;
	int send_socket;
	int recv_socket;
	int max_hops;
	int packet_size;
	int timeout;
	int flag_first_probe;
} t_sys;

int set_options(t_sys *sys, int ac, char **av);
int resolve_addr(t_sys *sys, const char *addr);
int prepare_sockets(t_sys *sys);
int set_ttl(t_sys *sys, int ttl);
void udp_probe(t_sys *sys, t_probe probe_type, int ttl);
